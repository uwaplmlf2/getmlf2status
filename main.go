// Getmlf2status retrieves the status information for an MLF2 float from
// the float database and writes it to standard output in CSV format.
package main

import (
	"flag"
	"fmt"
	"log"
	"net/url"
	"os"
	"runtime"
	"strconv"
	"strings"
	"text/template"
	"time"

	"bitbucket.org/uwaplmlf2/couchdb"
)

const (
	TFORMAT  = "2006-01-02T15:04:05-07:00"
	DBSERVER = "mlf2data.apl.uw.edu:443"
	DBNAME   = "mlf2db"
	VIEW     = "status/status"
	TEMPLATE = "{{.T|formatTime}},{{.Gps.Lat}},{{.Gps.Lon}},{{.Gps.Nsat}}"
)

var Version = "dev"
var BuildDate = "unknown"

var usage = `Usage: getmlf2status [options] floatid starttime [endtime]

Lookup the status information for an MLF2 float between starttime and
endtime and write it to standard output in CSV format. The default
variables output are Lat and Lon but this can be changed with the '-f'
option.

Both time strings must be in the following format:

YYYY-mm-dd[THH:MM[:SS]]

Missing time components are assumed to be zero. If endtime is not specified,
it defaults to now.
`

var (
	showvers = flag.Bool("version", false,
		"Show program version information and exit")
	debug    = flag.Bool("debug", false, "Enable debugging output")
	dbServer = flag.String("address", DBSERVER, "MLF2 database server address")
	tfmt     = flag.String("f", TEMPLATE, "Output format template")
)

// Template formatting function
func formatTime(t time.Time) string {
	return t.Truncate(time.Second).Format("2006-01-02 15:04:05Z")
}

// Parse_datetime is a flexible ISO8601-ish date/time parser. It handles
// the following formats (missing least-significant values are assumed
// to be zero):
//
//    YYYY-mm-ddTHH:MM:SS-ZZZZ
//    YYYY-mm-ddTHH:MM:SS   (UTC assumed)
//    YYYY-mm-ddTHH:MM
//    YYYY-mm-dd
//
func parse_datetime(dt string) (time.Time, error) {
	t, err := time.Parse("2006-01-02T15:04:05-0700", dt)
	if err != nil {
		t, err = time.Parse("2006-01-02T15:04:05", dt)
		if err != nil {
			t, err = time.Parse("2006-01-02T15:04", dt)
			if err != nil {
				t, err = time.Parse("2006-01-02", dt)
			}
		}
	}

	return t, err
}

func dbLookup(db *couchdb.Database, view string,
	floatid int, tstart, tend time.Time) (<-chan Status, error) {
	opts := make(map[string]interface{})
	opts["descending"] = false
	opts["include_docs"] = true
	opts["startkey"] = []interface{}{floatid, tstart.Format(TFORMAT)}
	opts["endkey"] = []interface{}{floatid, tend.Format(TFORMAT)}

	rows, err := db.Query(couchdb.NewView(view), opts)
	if err != nil {
		return nil, err
	}

	ch := make(chan Status, 1)
	go func() {
		defer close(ch)
		for rows.Next() {
			doc := Status{}
			err := rows.ScanDoc(&doc)
			if err != nil {
				log.Printf("Document decode error: %v", err)
				return
			}
			ch <- doc
		}
	}()

	return ch, nil
}

func main() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, usage)
		flag.PrintDefaults()
	}

	flag.Parse()
	if *showvers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	args := flag.Args()
	if len(args) < 2 {
		flag.Usage()
		os.Exit(1)
	}

	var (
		t_start, t_end time.Time
		err            error
	)

	floatid, _ := strconv.Atoi(args[0])
	t_start, err = parse_datetime(args[1])
	if err != nil {
		log.Fatalf("Bad start time: %q (%v)", args[1], err)
	}

	if len(args) > 2 {
		t_end, err = parse_datetime(args[2])
		if err != nil {
			log.Fatalf("Bad end time: %q (%v)", args[2], err)
		}
	} else {
		t_end = time.Now().UTC()
	}

	funcMap := template.FuncMap{
		"formatTime": formatTime,
	}
	tmpl, err := template.New("output").Funcs(funcMap).Parse(*tfmt + "\n")
	if err != nil {
		log.Fatalf("Invalid template: %q (%v)", *tfmt, err)
	}

	u := url.URL{}
	u.Host = *dbServer
	if strings.HasSuffix(*dbServer, ":443") {
		u.Scheme = "https"
	} else {
		u.Scheme = "http"
	}
	db := couchdb.NewDatabase(u.String(), DBNAME)
	if db == nil {
		log.Fatal("Database URL error")
	}

	ch, err := dbLookup(db, VIEW, floatid, t_start, t_end)
	if err != nil {
		log.Fatalf("Database lookup failed: %v", err)
	}

	for doc := range ch {
		tmpl.Execute(os.Stdout, doc)
	}
}
