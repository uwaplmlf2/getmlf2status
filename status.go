package main

import "time"

type GpsData struct {
	Nsat int     `json:"nsat"`
	Lat  float64 `json:"lat"`
	Lon  float64 `json:"lon"`
}

type SensorData struct {
	Ipr      float32 `json:"ipr"`
	Humidity float32 `json:"rh"`
	Voltage  []int   `json:"voltage"`
}

type Status struct {
	ID          string     `json:"_id,omitempty"`
	Rev         string     `json:"_rev,omitempty"`
	Type        string     `json:"type,omitempty"`
	Received    int64      `json:"t_recv,omitempty"`
	Floatid     int        `json:"floatid,omitempty"`
	T           time.Time  `json:"timestamp,omitempty"`
	Mode        string     `json:"mode"`
	Gps         GpsData    `json:"gps"`
	Sensors     SensorData `json:"sensors"`
	Irsq        int        `json:"irsq"`
	PistonError int        `json:"piston_error"`
	Freespace   int        `json:"freespace"`
	Alerts      []string   `json:"alerts,omitempty"`
}
