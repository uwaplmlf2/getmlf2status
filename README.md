# MLF2 Status Downloader

This application will download status messages from a specific MLF2 float over a specific time range and output the GPS position information in CSV format.

## Installation

Download a binary release for Linux, MacOS (darwin), or Windows from the [Downloads section](https://bitbucket.org/uwaplmlf2/getmlf2status/downloads/) of this repository and extract onto your system. All binaries are distributed in gzip-compressed TAR archives.

## Usage

``` shellsession
$ getmlf2status --help
Usage: getmlf2status [options] floatid starttime [endtime]

Lookup the status information for an MLF2 float between starttime and
endtime and write it to standard output in CSV format. The default
variables output are Lat and Lon but this can be changed with the '-f'
option.

Both time strings must be in the following format:

YYYY-mm-dd[THH:MM[:SS]]

Missing time components are assumed to be zero. If endtime is not specified,
it defaults to now.
  -address string
    	MLF2 database server address (default "mlf2data.apl.uw.edu:443")
  -debug
    	Enable debugging output
  -f string
    	Output format template (default "{{.T|formatTime}},{{.Gps.Lat}},{{.Gps.Lon}},{{.Gps.Nsat}}")
  -version
    	Show program version information and exit
```

## Examples

### Default Output

``` shellsession
$ getmlf2status 92 2018-11-30 2018-12-01
2018-11-30 03:54:45Z,50.892265,-141.874451,5
2018-11-30 09:55:07Z,50.876431,-141.843414,4
2018-11-30 15:30:05Z,50.875015,-141.856171,9
2018-11-30 21:28:12Z,50.876011,-141.82486,6
```

### Custom Output

The following message fields can be specified in the output template, you should always include the timestamp, `{{.T|formatTime}}`


| **Name**          | **Description**                          |
|-------------------|------------------------------------------|
| .Gps.Lat          | GPS latitude in degrees                  |
| .Gps.Lon          | GPS longitude in degrees                 |
| .Gps.Nsat         | Satellites in view                       |
| .Mode             | Operational mode; "recover" or "mission" |
| .Sensors.Ipr      | Internal pressure in psi                 |
| .Sensors.Humidity | Internal relative humidity in %          |
| .Sensors.Voltage  | Battery voltage                          |
| .PistonError      | Most recent piston error in counts       |
| .Freespace        | CF card free space in bytes              |
| .Alerts           | Alert messages                                         |

``` shellsession
$ getmlf2status -f "{{.T|formatTime}},{{.Gps.Lat}},{{.Gps.Lon}},{{.Gps.Nsat}},{{.Sensors.Ipr}}" 92 2018-11-30 2018-12-01
2018-11-30 03:54:45Z,50.892265,-141.874451,5,7.5
2018-11-30 09:55:07Z,50.876431,-141.843414,4,7.5
2018-11-30 15:30:05Z,50.875015,-141.856171,9,7.5
2018-11-30 21:28:12Z,50.876011,-141.82486,6,7.5
```
